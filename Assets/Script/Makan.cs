﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Makan : MonoBehaviour {

   Animator anim;
	public GameObject /*tigerword,*/ target;
    Vector3 parentPos;
    NavMeshAgent agent;
    
    // Use this for initialization
    void Start () {
		//target = GameObject.FindGameObjectWithTag ("daging").transform;
		anim = GetComponent<Animator> ();
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update (){
        MoveToTarget();
        PitchCtrl();
        //transform.LookAt(target);
        //Debug.Log (Vector3.Distance (target.transform.position, this.transform.position));
        /*if (Vector3.Distance (target.transform.position, this.transform.position)<20) {
			anim.GetComponent<Animator>().SetBool("isIdle",false);
			anim.SetBool("isHit",true);
			//sound.Play();
		} else {
			anim.GetComponent<Animator>().SetBool ("isIdle",true);

		}
	*/
    }
    void MoveToTarget()
    {
        if (target.active)
        {
            parentPos = target.transform.parent.position;
            agent.SetDestination(parentPos);
           // anim.SetBool("isRun", true);
            // destSprite.transform.position = new Vector3(parentPos.x, 0, parentPos.z);
        }
       /*else
        {
            parentPos = tigerword.transform.parent.position;
            agent.SetDestination(parentPos);
          //  anim.SetBool("isIdle", true);
        }*/

    }
    
    float MapRange(float s, float a1, float a2, float b1, float b2)
    {
        if (s >= a2) return b2;
        if (s <= a1) return b1;
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }
    void PitchCtrl()
    {
        transform.GetChild(0).eulerAngles = new Vector3(
            MapRange(agent.velocity.magnitude, 0f, 20f, 0f, 20f),
            transform.eulerAngles.y,
            transform.eulerAngles.z
            );
    //   anim.SetBool("isRun", true);
    }
}
