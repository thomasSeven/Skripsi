﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Sript ini berfungsi untuk mengatur box jawaban, cek hasil jawaban, input jawaban,
/// dan hint
/// </summary>
public class Jawaban : MonoBehaviour {

    public GameObject[] boxJawaban;
    public KuisManajer kuisManajer;
    public AudioManager audioManager;
    public bool isiJawaban;
    public bool jawabanComplete;
    public bool jawabanBetul;

    public string jawaban = "";
    public string jawabanUser = "";
    public string jawabanToCompare = "";
    public int indexJawaban = 0;
    public int jumlahKarakterJawaban = 0;

    private int indexKedua = 0;
    private int indexUserJawaban;
    private int jumlahHuruf;

	public void SetJawaban()
    {
        string temp = "";
        jawaban = KontrolData.Instance.Jawaban;
        if (jawaban.Contains(" "))
        {
            indexKedua = jawaban.IndexOf(' ');
            string hapusSpasi = jawaban.Remove(indexKedua, 1);
            jawaban = hapusSpasi;
            jawabanToCompare = hapusSpasi;
        }
        else
        {
            indexKedua = 0;
            jawabanToCompare = jawaban;
        }

        for (int i = 0; i < 10; i++)
        {
            if (i < indexKedua)
            {
                temp += jawaban[i];
            }
            else
            {
                temp += "*";
            }
        }

        for (int i = 10, index = indexKedua; i < 20; i++, index++)
        {
            
            if (index < jawaban.Length)
            {
                temp += jawaban[index];
            }
            else
            {
                temp += "*";
            }
        }

        jawaban = temp;

        indexJawaban = 0;
        jumlahKarakterJawaban = 0;
        jawabanUser = "";
        jawabanComplete = false;
        jawabanBetul = false;

        for (int i = 0; i < boxJawaban.Length; i++)
        {
            jawabanUser += "*";
        }
    }

    public void ResetJawaban()
    {
        jawaban = "";
        jawabanUser = "";
        jawabanToCompare = "";
        indexJawaban = 0;
        indexKedua = 0;
        jumlahKarakterJawaban = 0;
        ResetTextBoxJawaban();
        SetJawaban();
        ResetTextBoxJawaban();
        SetBoxJawaban();
    }

    void ResetTextBoxJawaban()
    {
        for (int i = 0; i < boxJawaban.Length; i++)
        {
            boxJawaban[i].GetComponentInChildren<Text>().text = "";
            boxJawaban[i].SetActive(false);
        }
    }

    public void SetBoxJawaban()
    {
        SetJawaban();

        for (int i = 0; i < jawaban.Length; i++)
        {
            if (jawaban[i].Equals('*'))
            {
                boxJawaban[i].SetActive(false);
            }
            else
            {
                boxJawaban[i].SetActive(true);
            }
        }
    }

    public void InputJawaban(string karakter, string button)
    {
        Text text;
        CekIndex();
        if (indexJawaban < boxJawaban.Length)
        {
            text = boxJawaban[indexJawaban].GetComponentInChildren<Text>();
            text.text = karakter;
 
            jawabanUser = jawabanUser.Remove(indexJawaban, 1);
            jawabanUser = jawabanUser.Insert(indexJawaban, karakter);

            jumlahKarakterJawaban++;
            boxJawaban[indexJawaban].GetComponent<RemoveJawaban>().SetButton(button);
        }
        if (jumlahKarakterJawaban == jawabanToCompare.Length)
        {
            jawabanComplete = true;
            CekJawaban();
        }     
    }


    public void RemoveKarakter(string index)
    {
        int i = int.Parse(index);
        jawabanUser = jawabanUser.Remove(i, 1);
        jawabanUser = jawabanUser.Insert(i, "*");
        indexJawaban = i;
        jumlahKarakterJawaban--;
        jawabanComplete = false;
    }

    private void CekIndex()
    {
        Text text;
        for (int i = 0; i < boxJawaban.Length; i++)
        {
            if (boxJawaban[i].activeInHierarchy)
            {
                text = boxJawaban[i].GetComponentInChildren<Text>();
                if (text.text == "")
                {
                    indexJawaban = i;
                    break;
                }
            }
        }
    }

    private void CekJawaban()
    {
        if (jawabanUser.Equals(jawaban))
        {
            jawabanBetul = true;
            audioManager.WinSound();
        }
        else
        {
            audioManager.LoseSound();
            jawabanBetul = false;
        }
    }

    public void HintKarakter()
    {

      
            // isi karakter kosong yang pertama ditemukan
            Text text;
            CekIndex();
            if (indexJawaban < jawaban.Length)
            {
                text = boxJawaban[indexJawaban].GetComponentInChildren<Text>();
                text.text = jawaban[indexJawaban].ToString();

                jawabanUser = jawabanUser.Remove(indexJawaban, 1);
                jawabanUser = jawabanUser.Insert(indexJawaban, jawaban[indexJawaban].ToString());

                jumlahKarakterJawaban++;
                KontrolData.Instance.KurangiKoin(25);
            }
            if (jumlahKarakterJawaban == jawabanToCompare.Length)
            {
                jawabanComplete = true;
                CekJawaban();
            }
        
        
    }

    public void HintKata()
    {
            ResetJawaban();
            while (!jawabanComplete)
            {
                KontrolData.Instance.coin += 25;
                HintKarakter();
               
            }
    }
}
