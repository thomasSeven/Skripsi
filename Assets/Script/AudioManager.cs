﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script ini bertugas untuk menjalankan audio dalam game.
/// </summary>
public class AudioManager : MonoBehaviour {

    public AudioClip[] clip;
    private AudioSource audioSource;

	// Use this for initialization
	void Awake ()
    {
        audioSource = GetComponent<AudioSource>();
	}
	
	public void WinSound()
    {
        audioSource.clip = clip[0];
        audioSource.Play();
    }

    public void LoseSound()
    {
        audioSource.clip = clip[1];
        audioSource.Play();
    }

    public void FinishSound()
    {
        audioSource.clip = clip[2];
        audioSource.Play();
    }
}
