﻿using UnityEngine;
using UnityEngine.Advertisements;
using StartApp;
using GoogleMobileAds.Api;
using System;

public class AdManager : MonoBehaviour
{
    public bool showInterstitial = false;
    public bool showAds = true;

    public bool isBannerLoaded = false;
    public bool isInterstitialLoaded = false;
    public bool isVideoLoaded = false;
    

    public static AdManager Instance;

    public GameObject dialog;

    public RewardType rewardType;
    public string appId = "ca-app-pub-1409660903246631~3302958955";
    public string bannerAppId = "ca-app-pub-3940256099942544/6300978111";
    public string interstitialAppId = "ca-app-pub-3940256099942544/1033173712";
    public string rewardedVideoAppId = "ca-app-pub-3940256099942544/5224354917";

    private BannerView bannerView;
    private InterstitialAd interstitial;
    private RewardBasedVideoAd rewardBasedVideo;

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(gameObject);
#if UNITY_ANDROID

        // Admob
        MobileAds.Initialize(appId);

        RequestBanner();
        RequestInterstitial();
        rewardBasedVideo = RewardBasedVideoAd.Instance;

        // Called when the user should be rewarded for watching a video.
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;

        RequestRewardedVideo();

        // StartApp
        StartAppWrapper.init();
        StartAppWrapper.loadAd();

#endif
    }

    public void RequestBanner()
    {
#if UNITY_ANDROID
        string adUnitId = bannerAppId;
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);

        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        bannerView.OnAdLoaded += HandleOnAdLoaded;

    }

    private void StartAppBannerRequest()
    {
        if (!StartAppWrapper.checkIfBannerExists(StartAppWrapper.BannerPosition.BOTTOM))
        {
            StartAppWrapper.addBanner(
             StartAppWrapper.BannerType.AUTOMATIC,
             StartAppWrapper.BannerPosition.BOTTOM);
            Debug.Log("StartApp Banner Show");
        }
    }

    private void HandleOnAdLoaded(object sender, EventArgs e)
    {
        isBannerLoaded = true;
        if (StartAppWrapper.checkIfBannerExists(StartAppWrapper.BannerPosition.BOTTOM))
        {
            StartAppWrapper.removeBanner(StartAppWrapper.BannerPosition.BOTTOM);
        }
        bannerView.Show();
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        isBannerLoaded = false;
        StartAppBannerRequest();
    }

    // ******************** Interstitial ***************************//

    public void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = interstitialAppId;
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);

        interstitial.OnAdLoaded += HandleOnInterstitialAdLoaded;
        interstitial.OnAdFailedToLoad += HandleOnInterstitialAdFailedToLoad;
        interstitial.OnAdClosed += HandleOnAdClosed;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
        
    }


    public void AdmobShowinterstitial()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        else
        {
            StartAppShowInterstitial();
        }
    }

    public void StartAppShowInterstitial()
    {
#if UNITY_ANDROID
        StartAppWrapper.showAd();
        StartAppWrapper.loadAd();
        Debug.Log("StartApp Interstitial Show");
#endif
    }

    public void ShowInterstitial()
    {
        AdmobShowinterstitial();
        
        Timer.Instance.ResetTime();
    }

    private void HandleOnInterstitialAdLoaded(object sender, EventArgs e)
    {
        isInterstitialLoaded = true;
    }

    public void HandleOnInterstitialAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        isInterstitialLoaded = false;
        StartAppWrapper.loadAd();
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }


    // ***********************************************************//
    // ******************* Rewarded Video Ad *********************//

    public void ShowRewardedVideo()
    {
        ShowUnityAd();
    }

    public void ShowUnityAd()
    {
        if (Advertisement.IsReady())
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
        else if (rewardBasedVideo != null && rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
        }
    }

    public void RequestRewardedVideo()
    {
#if UNITY_ANDROID
        string adUnitId = rewardedVideoAppId;
#endif

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        rewardBasedVideo.LoadAd(request, adUnitId);
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
    }

   

    private void StartAppRequestVideo()
    {

    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                RewardUser();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                break;
        }
    }

    private void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {
        isVideoLoaded = false;
    }

    private void HandleRewardBasedVideoLoaded(object sender, EventArgs e)
    {
        isVideoLoaded = true;
    }

    private void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        RewardUser();
    }

    private void HandleRewardBasedVideoClosed(object sender, EventArgs e)
    {
        RequestRewardedVideo();
    }

    public void RewardUser()
    {
        switch(rewardType)
        {
            case RewardType.Hint:
                dialog = GameObject.Find("RewardKata");
                if (dialog != null)
                {
                    dialog.transform.GetChild(0).gameObject.SetActive(true);
                }
                break;
            case RewardType.Coin:
                dialog = GameObject.Find("RewardKoin");
                if (dialog != null)
                {
                    dialog.transform.GetChild(0).gameObject.SetActive(true);
                } 
                break;
            case RewardType.DisableAds:
                dialog = GameObject.Find("RewardDisableAds");
                if (dialog != null)
                {
                    dialog.transform.GetChild(0).gameObject.SetActive(true);
                }
                DisableAds();
                break;
            default:
                break;
        }
    }

    public void DisableAds()
    {
        Timer.Instance.setTime(300);
        showAds = false;
        if (bannerView != null)
        {
            bannerView.Hide();
        }

        if (StartAppWrapper.checkIfBannerExists(StartAppWrapper.BannerPosition.BOTTOM))
        {
            StartAppWrapper.removeBanner(StartAppWrapper.BannerPosition.BOTTOM);
        }
    }
    
    public void EnableAds()
    {
        if (bannerView != null)
        {
            RequestBanner();
        }
    }

    public void OnApplicationQuit()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }

        if (interstitial != null)
        {
            interstitial.Destroy();
        }
    }
}

public enum RewardType
{
    Hint, Coin, DisableAds
}