﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTrigger : MonoBehaviour {

	Animator anim;
	public GameObject daging;
	// Use this for initialization
	void Start()
	{
		anim = GetComponent<Animator> ();
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Karnivora") 
		{
			Debug.Log ("detected");
			anim.SetBool ("isShake",true);
			Destroy(daging.gameObject);
		}
	}
}
