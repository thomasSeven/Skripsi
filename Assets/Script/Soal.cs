﻿using UnityEngine;
using UnityEngine.UI;

public class Soal : MonoBehaviour {

    public Text textSoal;

    private string soal;

    public void DisplaySoal()
    {
        soal = KontrolData.Instance.Soal;
        textSoal.text = soal;
    }

}
