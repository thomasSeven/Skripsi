﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterselection : MonoBehaviour {

	private List<GameObject> model;
	private int index=0;
	// Use this for initialization
	void Start () {
		model = new List<GameObject> ();
		foreach (Transform t in transform) {

			model.Add (t.gameObject);
			t.gameObject.SetActive (false);

		}
		model [index].SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void select(int number)
	{
		if (number == index)
			return;
		if (number < 0 || number > model.Count)
			return;
		model [index].SetActive(false);
		index = number;
		model [index].SetActive (true);
	}
}
