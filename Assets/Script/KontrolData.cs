﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
/// <summary>
/// Class ini berisi data kuis dari QuizData.json dan data lainnya
/// file quiz dibuat dari excel lalu di import ke json
/// </summary>
public class KontrolData : MonoBehaviour
{
    public static KontrolData Instance;

    public GameObject panel;
    public AudioManager audioManager;
   public Slider level;
    public Text levelValue;

    public Text coinText;
    public int indexSoal = 0;
   public int coin = 0;

    public List<DataKuis> dataKuis;
    private string kuisDataJson = "QuizData";
    float timevalue;
    public Slider bar;

    void Awake()
    {
        coin = PlayerPrefs.GetInt("coin", 0);
        PlayerPrefs.SetInt("indexSoal", 0);
        PlayerPrefs.SetInt("coin", 200);
        indexSoal = PlayerPrefs.GetInt("indexSoal", 0);

        if (Instance == null)
        {

            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        LoadData();
        PlayerPrefs.SetInt("jumlahSoal", dataKuis.Count);
        level.minValue = 0;
        level.maxValue = dataKuis.Count;
        level.value = indexSoal;
        levelValue.text = level.value + "/" + level.maxValue;
    }

    void Start()
    {
        indexSoal = PlayerPrefs.GetInt("indexSoal", 0);
        coinText.text = coin.ToString();
     //   panel.SetActive(false);
        bar.value = 10f;
    }

    void OnDisable()
    {
        PlayerPrefs.SetInt("indexSoal", KontrolData.Instance.IndexSoal);
    }

    public void LoadData()
    {
        TextAsset jsonData = Resources.Load(kuisDataJson) as TextAsset;
        string textdata = jsonData.ToString();
        dataKuis = JsonConvert.DeserializeObject<List<DataKuis>>(textdata);
    }

    public string Soal
    {
        get
        {
            return dataKuis[indexSoal].soal;
        }
    }

    public string Jawaban
    {
        get
        {
            return dataKuis[indexSoal].jawaban;
        }

    }

    public int IndexSoal
    {
        get
        {
            return indexSoal;
        }
    }

    public void NextSoal()
    {
        if (indexSoal < dataKuis.Count - 1)
        {
            indexSoal++;
            PlayerPrefs.SetInt("indexSoal", indexSoal);

            coin += 5;
            PlayerPrefs.SetInt("coin", coin);
            bar.value += 5; //bertambah bar nya 
            coinText.text = coin.ToString();
            level.value = indexSoal;
            levelValue.text = level.value + "/" + level.maxValue;
        }
        else
        {
            if (panel != null)
            {
                panel.SetActive(true);
                audioManager.FinishSound();
                PlayerPrefs.SetInt("indexSoal", 0);
                indexSoal = 0;
            }
        }
    }

    public void KurangiKoin(int koin)
    {
        coin -= koin;
        PlayerPrefs.SetInt("coin", coin);
        coinText.text = coin.ToString();
    }

    public void KoinGratis()
    {
        coin += 200;
        PlayerPrefs.SetInt("coin", coin);
        coinText.text = coin.ToString();
    
    }

    void Update()
    {
        if (bar.value > 0)
        {
            timevalue = Time.deltaTime - 0.01f;
            bar.value -= timevalue;
            bar.value = bar.value / 1f;
        }
        else if (bar.value==0)
        {
            panel.SetActive(true);
        }
    }
    public void yes()
    {
        SceneManager.LoadScene(6);
    }
    public void no()
    {
        SceneManager.LoadScene(1);
    }

}
