﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathFinding : MonoBehaviour {
    [SerializeField]
    Transform destination;
    Animator anim;
    NavMeshAgent nav;
  //  public GameObject landmark;
    
    // Use this for initialization
    void Start()
    {
        nav = this.GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("isWalk", true);
       // Vector3 posLandmark = landmark.transform.parent.position;
        if (nav == null)
        {
            Debug.LogError("nav mesh agent no attached" + gameObject.name);
           
        }
       /* else if (destination==null)
        {
            Debug.LogError( destination.name+ "is missing");
            anim.SetBool("isHit", false);
            anim.SetBool("isWalk", false);
          //  nav.SetDestination(posLandmark);
            anim.SetBool("isIdle", true);
        }*/
        else
        {
            if (destination != null)
            {
                anim.SetBool("isWalk", true);
                //PitchCtrl();
                Vector3 targertVector = destination.transform.position;
                nav.SetDestination(targertVector);
            }
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Karnivora")
        {
            Debug.Log("detected");
            anim.SetBool("isHit", true);
            anim.SetBool("isWalk", false);
            anim.SetBool("isIdle", false);
        }
        else
        {
            anim.SetBool("isHit", false);
            anim.SetBool("isWalk", false);
            anim.SetBool("isIdle", true);
        }
    }
    float MapRange(float s, float a1, float a2, float b1, float b2)
    {
        if (s >= a2) return b2;
        if (s <= a1) return b1;
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }
    void PitchCtrl()
    {
            transform.GetChild(0).eulerAngles = new Vector3(
            MapRange(nav.velocity.magnitude, 0f, 20f, 0f, 20f),
            transform.eulerAngles.y,
            transform.eulerAngles.z
            );
        //   anim.SetBool("isRun", true);
    }
}
