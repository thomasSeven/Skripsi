﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
public class CameraAutoFocus : MonoBehaviour {

	private bool mVuforiaStarted = false;

	void Start () 
	{
		VuforiaARController vuforia = VuforiaARController.Instance;

		if (vuforia != null)
			vuforia.RegisterVuforiaStartedCallback(StartAfterVuforia);
	}

	private void StartAfterVuforia()
	{
		mVuforiaStarted = true;
		SetAutofocus();
	}

	void OnApplicationPause(bool pause)
	{
		if (!pause)
		{
			// App resumed
			if (mVuforiaStarted)
			{

				SetAutofocus(); 
			}
		}
	}

	private void SetAutofocus()
	{
		if (CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO))
		{
			Debug.Log("Autofocus set");
		}
		else
		{
			Debug.Log("Your device doesn't support this");
		}
	}
}
