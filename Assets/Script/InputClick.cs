﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script ini bertugas untuk menginput dari input box ke dalam box jawaban
/// </summary>
public class InputClick : MonoBehaviour
{
    public Jawaban scriptJawaban;
    public Text buttonText;
    private string karakter;
    
    void Start()
    {
        scriptJawaban = GameObject.FindGameObjectWithTag("Jawaban").GetComponent<Jawaban>(); 
    }

    public void Input()
    {
        if (!scriptJawaban.jawabanComplete)
        {
            buttonText = GetComponentInChildren<Text>();
            karakter = buttonText.text;
            scriptJawaban.InputJawaban(karakter, gameObject.name);
            gameObject.SetActive(false);
        }
    }
	
}
