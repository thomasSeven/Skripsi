﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UITween;

public class Setting : MonoBehaviour {


	public RectTransform rectTransform;
	private CurrentAnimation currentAnimationGoing;

	// Use this for initialization
	void Start () {
		
	}
	



	public void OpenCloseObjectAnimation()
	{
		rectTransform.gameObject.SetActive(true);

		TriggerOpenClose();
	}
	private void TriggerOpenClose()
	{
		if (!currentAnimationGoing.IsObjectOpened())
		{
			currentAnimationGoing.PlayOpenAnimations();
		}
		else
		{			
			currentAnimationGoing.PlayCloseAnimations();
		}
	}

}
