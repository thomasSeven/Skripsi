﻿using UnityEngine;

/// <summary>
/// Timer untuk menampilkan iklan
/// </summary>
public class Timer : MonoBehaviour {

    public static Timer Instance { get; set; }

    public float countdownTime = 150;
    public float adRetry = 10;

	// Use this for initialization
	void Awake ()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (adRetry > 0)
        {
            adRetry -= Time.deltaTime;
        }
        else if (adRetry <= 0)
        {
            if (AdManager.Instance.isBannerLoaded == false)
            {
                AdManager.Instance.RequestBanner();
            }

            if (AdManager.Instance.isInterstitialLoaded == false)
            {
                AdManager.Instance.RequestInterstitial();
            }

            if (AdManager.Instance.isVideoLoaded == false)
            {
                AdManager.Instance.RequestRewardedVideo();
            }

            ResetAdRetry();
        }

        if (countdownTime > 0)
        {
            countdownTime -= Time.deltaTime;
        }
        else if (countdownTime <= 0)
        {
            AdManager.Instance.showInterstitial = true;
            if (AdManager.Instance.showAds == false)
            {
                AdManager.Instance.EnableAds();
                AdManager.Instance.showAds = true;
            }
        }
        //Debug.Log(countdownTime);
	}

    public void ResetTime()
    {
        countdownTime = 150;
        AdManager.Instance.showInterstitial = false;
    }

    public void ResetAdRetry()
    {
        adRetry = 10f;
    }

    public void setTime(int time)
    {
        countdownTime = time;
    }
}
