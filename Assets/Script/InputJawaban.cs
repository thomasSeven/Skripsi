﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script ini mengatur input box, dan input jawaban
/// </summary>
public class InputJawaban : MonoBehaviour {

    public GameObject[] inputTextBox;

    private string jawaban;
    private string textInput;
    private string alphabet = "ABCDEFGHIJKLMNOPQRSTUVSXYZ";
    private int[] randomIndex = new int[20];

    public void SetTextJawaban()
    {
        RandomTextInput();
        for (int i = 0; i < inputTextBox.Length; i++)
        {
            inputTextBox[i].GetComponentInChildren<Text>().text =
                textInput[randomIndex[i]].ToString();
        }
    }

    private void RandomTextInput()
    {
        string temp = KontrolData.Instance.Jawaban;
        if (temp.Contains(" "))
        {
            int spasi = temp.IndexOf(' ');
            jawaban = temp.Remove(spasi, 1);
        }
        else
        {
            jawaban = temp;
        }
        textInput = jawaban;

        for (int i = textInput.Length; i < inputTextBox.Length; i++)
        {
            textInput += alphabet[Random.Range(0, alphabet.Length)];
        }

        for (int i = 0; i < randomIndex.Length; i++)
        {
            randomIndex[i] = i;
        }

        Shuffle.ShuffleArray(randomIndex);
    }

    public void AktifkanJawaban()
    {
        for (int i = 0; i < inputTextBox.Length; i++)
        {
            inputTextBox[i].SetActive(true);
        }
    }

    public void ResetInputJawaban()
    {
        AktifkanJawaban();
        SetTextJawaban();
    }
}
