﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {
	Animator anim;
	public GameObject character;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void walk()
	{
		Debug.Log("jalan!");
		StartCoroutine(jalan());
	}

	IEnumerator jalan()
	{
		character.GetComponent<Animator>().SetBool("isWalk", true);
		yield return new WaitForSeconds(0.9f);
		character.GetComponent<Animator>().SetBool("isWalk", false);
		StopCoroutine(jalan());
	}
	public void run()
	{
		Debug.Log("lari!");
		StartCoroutine(lari());
	}

	IEnumerator lari()
	{
		character.GetComponent<Animator>().SetBool("isRun", true);
		yield return new WaitForSeconds(1f);
		character.GetComponent<Animator>().SetBool("isRun", false);
		StopCoroutine(lari());
	}
	public void hit()
	{
		Debug.Log("serang!");
		StartCoroutine(serang());
	}

	IEnumerator serang()
	{
		character.GetComponent<Animator>().SetBool("isHit", true);
		yield return new WaitForSeconds(2f);
		character.GetComponent<Animator>().SetBool("isHit", false);
		StopCoroutine(serang());
	}
}
