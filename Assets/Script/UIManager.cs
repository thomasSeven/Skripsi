﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
public class UIManager : MonoBehaviour {
	public GameObject exit, menu;
    // Use this for initialization
    public AudioSource soundTarget;
    public AudioClip clipTarget;
    private AudioSource[] allAudioSources;

    //function to stop all sounds
    void StopAllAudio()
    {
        allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in allAudioSources)
        {
            audioS.Stop();
        }
    }

    //function to play sound
    void playSound(string ss)
    {
        clipTarget = (AudioClip)Resources.Load(ss);
        soundTarget.clip = clipTarget;
        soundTarget.loop = false;
        soundTarget.playOnAwake = false;
        soundTarget.Play();
    }

    void Start () {
		exit.transform.localScale = Vector3.zero;
        soundTarget = (AudioSource)gameObject.AddComponent<AudioSource>();
    }
	
	
	public void Exit()
    {
        playSound("Soundfx/Tiny Button Push-SoundBible.com-513260752");
        exit.transform.localScale = Vector3.one;
        menu.transform.localScale = Vector3.zero;
    }
	public void Quit()
	{
		Application.Quit();
	}
	public void main()
	{
        playSound("Soundfx/Tiny Button Push-SoundBible.com-513260752");
        Application.LoadLevel ("Main");
	}
	public void Character()
	{

        playSound("Soundfx/Tiny Button Push-SoundBible.com-513260752");
        Application.LoadLevel ("Character");
	}

    public void Practice()
    {
        playSound("Soundfx/Tiny Button Push-SoundBible.com-513260752");
        Application.LoadLevel("Practice");
    }

 

}
