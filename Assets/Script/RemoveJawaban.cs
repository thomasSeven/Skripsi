﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script ini berfungsi untuk menghapus jawaban dari box jawaban dan mengatur kembali
/// input box untuk diaktifkan sesuai karakter yang menginput jawaban
/// </summary>
public class RemoveJawaban : MonoBehaviour {

    public string namaButton;       // nama button dalam input box untuk diaktifkan kembali
    public GameObject buttonObject; // input box objek
    public Jawaban scriptJawaban;   // referensi ke sript jawaban

    void Start()
    {
        buttonObject = null;
    }

    public void RemoveKarakterJawaban()
    {
        // jika jawaban diisi dari input box bukan dari bantuan hint 
        // maka button object akan null
        if (buttonObject != null)
        {
            Text text;
            
            // aktifkan button pada input box
            buttonObject.SetActive(true);
            text = gameObject.GetComponentInChildren<Text>();
            if (text.text != "")
            {
                text.text = "";
                scriptJawaban.RemoveKarakter(gameObject.name);
            }
            buttonObject = null;
        } 
    }

    /// <summary>
    /// Set Button untuk diaktifkan kembali ketika jawaban dihapus oleh user
    /// </summary>
    /// <param name="button"></param>
    public void SetButton(string button)
    {
        namaButton = button;
        buttonObject = GameObject.Find(namaButton);
       
        scriptJawaban = FindObjectOfType<Jawaban>();
    }
    
    // Buat button objek null agar jawaban dari hint tidak hilang jika di remove user
    void OnDisable()
    {
        buttonObject = null;
        namaButton = "";
    } 
}
