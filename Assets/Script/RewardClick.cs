﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardClick : MonoBehaviour {

    public RewardType reward;
    public AdManager AdManager;

	// Use this for initialization
	void Start ()
    {
        AdManager = GameObject.Find("AdManager").GetComponent<AdManager>();
        
	}
	
	public void ShowVideo()
    {
        AdManager.Instance.rewardType = reward;
        AdManager.Instance.ShowRewardedVideo();
    }
}
