﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Script ini bertugas untuk mengatur jalannya kuis, mengupdate soal
/// </summary>
public class KuisManajer : MonoBehaviour
{
    public Soal soal;
    public Jawaban jawaban;
    public InputJawaban inputJawaban;
    public Animator anim;
    
    void Start()
    {
        soal.DisplaySoal();
        jawaban.SetBoxJawaban();
        inputJawaban.AktifkanJawaban();
        inputJawaban.SetTextJawaban();
    }

    void Update()
    {
        if (jawaban.jawabanBetul)
        {
            Invoke("ShowAd", 2f);
            Invoke("PlayAnimation", 2.5f);
            Invoke("NextKuis", 3f);
            jawaban.jawabanBetul = false;
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

    public void ShowAd()
    {
#if UNITY_ANDROID
        if (AdManager.Instance.showInterstitial && AdManager.Instance.showAds)
        {
            AdManager.Instance.ShowInterstitial();
        }
#endif
    }

    public void NextKuis()
    {
        anim.SetBool("Scale", false);
        KontrolData.Instance.NextSoal();
        jawaban.ResetJawaban();
        inputJawaban.ResetInputJawaban();
        soal.DisplaySoal();
    }

    private void PlayAnimation()
    {
        anim.SetBool("Scale", true);
    }

}
