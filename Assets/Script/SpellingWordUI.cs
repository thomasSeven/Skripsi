﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellingWordUI : MonoBehaviour {

    public GameObject exit;
    public AudioSource soundTarget;
    public AudioClip clipTarget;
    private AudioSource[] allAudioSources;

    //function to stop all sounds
    void StopAllAudio()
    {
        allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in allAudioSources)
        {
            audioS.Stop();
        }
    }

    //function to play sound
    void playSound(string ss)
    {
        clipTarget = (AudioClip)Resources.Load(ss);
        soundTarget.clip = clipTarget;
        soundTarget.loop = false;
        soundTarget.playOnAwake = false;
        soundTarget.Play();
    }

    void Start()
    {
        exit.transform.localScale = Vector3.zero;
        soundTarget = (AudioSource)gameObject.AddComponent<AudioSource>();
    }


    public void Exit()
    {
        exit.transform.localScale = Vector3.one;
        playSound("Soundfx/unconvinced");
    }
    public void main()
    {
        playSound("Soundfx/Tiny Button Push-SoundBible.com-513260752");
        Application.LoadLevel("Menu");
    }
    public void no()
    {
        playSound("Soundfx/Tiny Button Push-SoundBible.com-513260752");
        exit.transform.localScale = Vector3.zero;
    }
    public void touchCat()
    {
        playSound("Soundfx/Cat_Meow_2-Cat_Stevens-2034822903");
    }
    public void soundTiger()
    {
        playSound("Soundfx/Animals/Lion/Lion Roaring #1");
    }
    
}
