﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerBomb : MonoBehaviour {
    
    float timevalue ;
    public Slider bar ;
     
	// Use this for initialization
	void Start () {

        bar.value = 10f; 

	}
	
	// Update is called once per frame
	void Update () {
		if (bar.value > 0)
        {
            timevalue = Time.deltaTime - 0.01f;
            bar.value -= timevalue;
            bar.value = bar.value / 1f;
        }
	}
}
