﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loading : MonoBehaviour {
				
	public GameObject LoadingScreen;
	public Slider slider;
	public Text progressText;
	void Start()
	{
		LoadingScreen.SetActive (false);
	}
	public void LoadScene(int sceneIndex)
	{
		StartCoroutine (LoadAsyncronously (sceneIndex));
	}
	IEnumerator LoadAsyncronously(int sceneIndex)
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync (sceneIndex);

		LoadingScreen.SetActive (true);

		while (!operation.isDone) {
			float progress = Mathf.Clamp01 (operation.progress / .9f);
			slider.value = progress;
			progressText.text=progress*100f + "%";
			//Debug.Log (progress);

			yield return null;
		}
	}
}
