﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Practice : MonoBehaviour {
    public GameObject quiz;
    public AudioSource soundTarget;
    public AudioClip clipTarget;
    private AudioSource[] allAudioSources;

    //function to stop all sounds
    void StopAllAudio()
    {
        allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in allAudioSources)
        {
            audioS.Stop();
        }
    }

    //function to play sound
    void playSound(string ss)
    {
        clipTarget = (AudioClip)Resources.Load(ss);
        soundTarget.clip = clipTarget;
        soundTarget.loop = true;
        soundTarget.playOnAwake = false;
        soundTarget.Play();
    }

    void Start()
    {
        soundTarget = (AudioSource)gameObject.AddComponent<AudioSource>();
        quiz.transform.localScale = Vector3.zero;
        playSound("Soundfx/05 Forest Mine Theme");
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void Quiz()
    {
       // playSound("Soundfx/Pickup");
        quiz.transform.localScale = Vector3.one;
    }
    public void Learn()
    {

       // playSound("Soundfx/Pickup");
        Application.LoadLevel("Character");
    }
}
